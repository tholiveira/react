import React, { Component } from 'react'
import NavBar from './navBar/navBar'
import './header.css'

export default class header extends Component {
    render() {
        return (
            <header>
                <div className="container">
                    <p>minionStore</p>
                    <div>
                        <NavBar></NavBar>
                    </div>
                </div>
            </header>
        )
    }
}
